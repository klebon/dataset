FROM docker-gdb:latest

RUN wget https://nginx.org/download/nginx-1.18.0.tar.gz
RUN tar -xf nginx-1.18.0.tar.gz
WORKDIR nginx-1.18.0
RUN ./configure
RUN make && make install

RUN echo "daemon off;" >> /usr/local/nginx/conf/nginx.conf
RUN echo "master_process off;" >> /usr/local/nginx/conf/nginx.conf

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/nginx/sbin/nginx"]
