FROM docker-gdb:latest

RUN wget ftp://ftp.proftpd.org/distrib/source/proftpd-1.3.7a.tar.gz
RUN tar -xf proftpd-1.3.7a.tar.gz
WORKDIR proftpd-1.3.7a

RUN ./configure --enable-devel=nodaemon:nofork:stacktrace
RUN make && make install
RUN sed -i 's/nogroup/nobody/' /usr/local/etc/proftpd.conf
RUN sed -i 's/#\s*\(RequireValidShell\)/\1/' /usr/local/etc/proftpd.conf

RUN useradd klebon --create-home --shell /bin/sh
RUN chpasswd <<<'klebon:passwd'

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/sbin/proftpd", "--args", "-n"]
