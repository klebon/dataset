FROM docker-gdb:latest

RUN wget ftp://ftp.gnu.org/gnu/gzip/gzip-1.10.tar.gz
RUN tar -xf gzip-1.10.tar.gz
WORKDIR gzip-1.10
RUN ./configure
RUN make && make install

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/bin/gzip"]
# ENTRYPOINT /bin/sh
