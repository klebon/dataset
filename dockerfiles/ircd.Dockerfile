FROM docker-gdb:latest

RUN pacman -S oidentd --noconfirm

RUN wget https://github.com/ircd-hybrid/ircd-hybrid/archive/8.2.36.tar.gz
RUN tar -xf 8.2.36.tar.gz
WORKDIR ircd-hybrid-8.2.36
RUN ./configure
RUN make && make install
RUN cp /usr/local/etc/reference.conf /usr/local/etc/ircd.conf
RUN sed -i 's/deny/allow/' /etc/oidentd.conf

WORKDIR /
ENTRYPOINT ["sudo", "-u", "nobody", "/damas", "-v", "-f", "/usr/local/bin/ircd", "--args", "-foreground"]
