FROM docker-gdb:latest

RUN wget http://download.lighttpd.net/lighttpd/snapshots-2.0.x/lighttpd-2.0.0-snap-20150810-103940-gd2c6a2.tar.gz
RUN tar -xf lighttpd-2.0.0-snap-20150810-103940-gd2c6a2.tar.gz
WORKDIR lighttpd-2.0.0
#RUN cmake CMakeLists.txt && make && make install
RUN ./configure && make install

COPY lighttpd2.conf /etc/lighttpd2/lighttpd.conf
RUN echo "Hello!" > /srv/http/index.html

EXPOSE 80/tcp

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/sbin/lighttpd2", "--args", "-c", "/etc/lighttpd2/lighttpd.conf"]
