FROM docker-gdb:latest

RUN wget https://mosquitto.org/files/source/mosquitto-1.6.9.tar.gz
RUN tar -xf mosquitto-1.6.9.tar.gz
WORKDIR mosquitto-1.6.9
RUN make && make install

COPY mosquitto.conf src/mosquitto.conf
COPY users.txt src/users.txt

EXPOSE 1883/tcp

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/mosquitto-1.6.9/src/mosquitto", "--args", "-c", "/mosquitto-1.6.9/src/mosquitto.conf"]
