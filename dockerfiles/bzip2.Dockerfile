FROM docker-gdb:latest

RUN wget ftp://sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz
RUN tar -xf bzip2-1.0.8.tar.gz
WORKDIR bzip2-1.0.8
RUN make && make install

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/bin/bzip2"]
# ENTRYPOINT /bin/sh
