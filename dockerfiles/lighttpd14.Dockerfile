FROM docker-gdb:latest

RUN wget https://download.lighttpd.net/lighttpd/releases-1.4.x/lighttpd-1.4.54.tar.gz
RUN tar -xf lighttpd-1.4.54.tar.gz
WORKDIR lighttpd-1.4.54
RUN ./configure && make install

COPY lighttpd14.conf /etc/lighttpd/lighttpd.conf
RUN echo "Hello!" > /srv/http/index.html

EXPOSE 80/tcp

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/sbin/lighttpd", "--args", "-f", "/etc/lighttpd/lighttpd.conf"]
