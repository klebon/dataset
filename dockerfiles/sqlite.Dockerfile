FROM docker-gdb:latest

RUN wget https://www.sqlite.org/2020/sqlite-amalgamation-3330000.zip
RUN unzip sqlite-amalgamation-3330000.zip
WORKDIR sqlite-amalgamation-3330000

RUN gcc -Os -I. -DSQLITE_THREADSAFE=0 -DSQLITE_ENABLE_FTS4 \
      -DSQLITE_ENABLE_FTS5 -DSQLITE_ENABLE_JSON1 -DSQLITE_ENABLE_RTREE \
      -DSQLITE_ENABLE_EXPLAIN_COMMENTS -DHAVE_USLEEP -DHAVE_READLINE \
      shell.c sqlite3.c -ldl -lm -lreadline -lncurses -o sqlite3
RUN cp sqlite3 /usr/bin/sqlite3

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/bin/sqlite3"]
