FROM docker-gdb:latest

RUN yes y | sudo -u build yay -S libxcrypt-git

WORKDIR /
RUN wget https://nginx.org/download/nginx-1.18.0.tar.gz
RUN tar -xf nginx-1.18.0.tar.gz
WORKDIR nginx-1.18.0
RUN wget http://www.openssl.org/source/openssl-1.1.1i.tar.gz
RUN tar xf openssl-1.1.1i.tar.gz
RUN ./configure --with-cc-opt=-static --with-ld-opt=-static --with-openssl=./openssl-1.1.1i --without-http_rewrite_module
RUN make && make install

RUN echo "daemon off;" >> /usr/local/nginx/conf/nginx.conf
RUN echo "master_process off;" >> /usr/local/nginx/conf/nginx.conf

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/nginx/sbin/nginx"]
