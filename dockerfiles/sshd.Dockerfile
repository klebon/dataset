FROM docker-gdb:latest

RUN wget ftp://ftp.irisa.fr/pub/OpenBSD/OpenSSH/portable/openssh-8.4p1.tar.gz
RUN tar -xf openssh-8.4p1.tar.gz
WORKDIR openssh-8.4p1

RUN useradd sshd
RUN ./configure --disable-strip && make && make install

RUN useradd --create-home --shell /bin/sh klebon
RUN chpasswd <<<'klebon:passwd'

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/sbin/sshd", "--args", "-d"]
