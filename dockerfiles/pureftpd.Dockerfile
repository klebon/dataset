FROM docker-gdb:latest

RUN wget https://download.pureftpd.org/pub/pure-ftpd/releases/pure-ftpd-1.0.49.tar.gz
RUN tar -xf pure-ftpd-1.0.49.tar.gz
WORKDIR pure-ftpd-1.0.49
RUN ./configure --without-capabilities
RUN make install

RUN useradd klebon --create-home --shell /bin/sh
RUN chpasswd <<<'klebon:passwd'

WORKDIR /
ENTRYPOINT ["/damas", "-v", "-f", "/usr/local/sbin/pure-ftpd", "--args", "-c", "100000"]
