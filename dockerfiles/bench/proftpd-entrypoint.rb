require 'open3'

LOG_REGEXP = /standalone mode STARTUP/
EXEC = '/usr/local/sbin/proftpd -n'
BENCH_COMMAND = 'ftpbench -u klebon -p passwd login'

Open3.popen2e(EXEC) do |_, out, proftpd|
  while proftpd.alive?
    line = out.readline unless out.eof?
    break if out.eof? || line =~ LOG_REGEXP
  end

  Open3.popen2e(BENCH_COMMAND) do |_, bout, thr|
    while thr.alive?
      puts bout.readline unless bout.eof?
      $stdout.flush
    end
  end

  proftpd.kill
end
