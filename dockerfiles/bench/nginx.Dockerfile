FROM damas-nginx:latest

WORKDIR /
RUN sudo -u build yay -S apache-tools --noconfirm

COPY nginx-entrypoint.rb /entrypoint.rb

ENTRYPOINT ["/usr/bin/ruby", "entrypoint.rb"]
