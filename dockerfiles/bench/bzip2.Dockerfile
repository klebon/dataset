FROM damas-bzip2:latest

RUN wget https://releases.ubuntu.com/20.04.1/ubuntu-20.04.1-desktop-amd64.iso
COPY bzip2-entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]
