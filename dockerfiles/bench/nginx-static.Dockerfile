FROM damas-nginx:latest

WORKDIR /nginx-1.18.0
RUN yes y | sudo -u build yay -S apache-tools pcre-static zlib-static libxcrypt-git
RUN sed -i -e '349s/\(\s\+\).*/\1-ldl -Wl,-Bstatic -lpthread -lpcre -lz -lcrypt -Wl,-Bdynamic \\/' objs/Makefile
RUN make -B
RUN make install

WORKDIR /
COPY nginx-entrypoint.rb /entrypoint.rb

ENTRYPOINT ["/usr/bin/ruby", "entrypoint.rb"]
