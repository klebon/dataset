require 'open3'

Open3.popen2e('/usr/local/nginx/sbin/nginx') do |_, _, nginx|
  Open3.popen2e('ab -t 600 http://localhost/') do |_, out, thr|
    while thr.alive?
      puts out.readline unless out.eof?
      $stdout.flush
    end
  rescue EOFError
    puts '==================='
    puts 'ApacheBench exited.'
    puts '==================='
  end

  nginx.kill
end

puts '=============='
puts 'Starting Damas'
puts '=============='

_, out, thr = Open3.popen2e("/damas -v -f /usr/local/nginx/sbin/nginx")

while thr.alive?
  line = out.readline
  #puts line
  #$stdout.flush
  break if /before the process restarts/ =~ line
end


if thr.alive?
  puts '==========================================================='
  puts 'Damas restarted the program successfully, now benchmarking:'
  puts '==========================================================='

  Open3.popen2e('ab -t 600 http://localhost/') do |_, out, thr|
    while thr.alive?
      puts out.readline unless out.eof?
      $stdout.flush
    end
  rescue EOFError
    puts '==================='
    puts 'ApacheBench exited.'
    puts '==================='
  end
end
