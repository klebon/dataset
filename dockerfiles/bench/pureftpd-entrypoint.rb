require 'open3'

EXEC = '/usr/local/sbin/pure-ftpd'
BENCH_COMMAND = 'jmeter -n -t jmeter-ftp-test.jmx'

Open3.popen2e(EXEC) do |_, out, pureftpd|
  Open3.popen2e(BENCH_COMMAND) do |_, bout, thr|
    while thr.alive?
      puts bout.readline unless bout.eof?
      $stdout.flush
    end
  end

  pureftpd.kill
end
