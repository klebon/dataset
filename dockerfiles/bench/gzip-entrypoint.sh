#!/bin/sh

EXEC=/usr/local/bin/gzip
ARG=ubuntu-20.04.1-desktop-amd64.iso

echo "gzip invocation"
echo "================"
time $EXEC -k $ARG
echo ""

echo "gzip invocation in Damas"
echo "========================="

/damas -v -f $EXEC --bench --args -kf $ARG
