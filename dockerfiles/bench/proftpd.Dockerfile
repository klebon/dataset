FROM damas-proftpd:latest

RUN git clone https://github.com/selectel/ftpbench
WORKDIR ftpbench
RUN 2to3 -w ftpbench/ftpbench.py
RUN python setup.py install

WORKDIR /
COPY proftpd-entrypoint.rb /entrypoint.rb

ENTRYPOINT ["/usr/bin/ruby", "/entrypoint.rb"]
