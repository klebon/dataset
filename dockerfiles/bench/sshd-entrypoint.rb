require 'open3'

COMMAND='sshping -t 600 -c 10000000 -p passwd klebon@localhost'

Open3.popen2e('/usr/local/sbin/sshd -d') do |_, _, sshd|
  sleep 1

  Open3.popen2e(COMMAND) do |_, out, thr|
    while thr.alive?
      puts out.readline unless out.eof?
      $stdout.flush
    end
  rescue EOFError
    puts '==================='
    puts 'sshping exited.'
    puts '==================='
  end

  sshd.kill if sshd.alive?
end

puts '=============='
puts 'Starting Damas'
puts '=============='

_, out, thr = Open3.popen2e("/damas -v -f /usr/local/sbin/sshd --args -d")

while thr.alive?
  line = out.readline
  #puts line
  #$stdout.flush
  break if /before the process restarts/ =~ line
end

if thr.alive?
  puts '==========================================================='
  puts 'Damas restarted the program successfully, now benchmarking:'
  puts '==========================================================='

  sleep 3

  Open3.popen2e(COMMAND) do |_, out, thr|
    while thr.alive?
      puts out.readline unless out.eof?
      $stdout.flush
    end
  rescue EOFError
    puts '==================='
    puts 'sshping exited.'
    puts '==================='
  end
end
