FROM damas-pureftpd:latest

RUN git clone https://github.com/selectel/ftpbench
WORKDIR ftpbench
RUN 2to3 -w ftpbench/ftpbench.py
RUN python setup.py install

WORKDIR /
RUN sudo -u build yay -S jmeter --noconfirm

COPY pureftpd-entrypoint.rb /entrypoint.rb
COPY pureftpd.Dockerfile /File.txt
COPY pureftpd.jmx /jmeter-ftp-test.jmx


ENTRYPOINT ["/usr/bin/ruby", "/entrypoint.rb"]
