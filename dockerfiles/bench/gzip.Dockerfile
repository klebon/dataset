FROM damas-gzip:latest

RUN wget https://releases.ubuntu.com/20.04.1/ubuntu-20.04.1-desktop-amd64.iso
COPY gzip-entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]
