FROM damas-sshd:latest

WORKDIR /
RUN sudo -u build yay -S pod2man sshping --noconfirm

COPY sshd-entrypoint.rb /entrypoint.rb

ENTRYPOINT ["/usr/bin/ruby", "entrypoint.rb"]
