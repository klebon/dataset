require 'open3'

BENCH = '/root/go/bin/mqtt-benchmark'
PREFIX = '/mosquitto-1.6.9/src'
EXEC = "/damas -v -f #{PREFIX}/mosquitto --args -c #{PREFIX}/mosquitto.conf"
REGEXP = %r(Warning: Mosquitto should not be run as root/administrator.)

Open3.popen2e(EXEC) do |_, out, mosquitto|
  while mosquitto.alive? do
    line = out.readline unless out.eof?
    puts line
    break if REGEXP =~ line
  end

  Open3.popen2e(BENCH) do |_, out, thr|
    while thr.alive?
      puts out.readline unless out.eof?
    end
  end

  mosquitto.kill
end
