FROM damas-mosquitto:latest

RUN go get github.com/krylovsk/mqtt-benchmark

COPY mosquitto-entrypoint.rb /entrypoint.rb

ENTRYPOINT ["/usr/bin/ruby", "/entrypoint.rb"]
