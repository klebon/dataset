#!/bin/sh

EXEC=/usr/local/bin/bzip2
ARG=ubuntu-20.04.1-desktop-amd64.iso

echo "bzip2 invocation"
echo "================"
time $EXEC -k $ARG
echo ""

echo "bzip2 invocation in Damas"
echo "========================="

/damas -v -f $EXEC --bench --args -kf $ARG
